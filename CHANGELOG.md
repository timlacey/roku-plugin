## [6.5.14] - 2021-03-31
### Added
- Support for `CMF` transport format

## [6.5.13] - 2021-02-26
### Fixed
- Init being sent after start breaking the logic of the views.

## [6.5.12] - 2021-02-09
### Removed
- Logs by default, now all are hidden

## [6.5.11] - 2020-12-29
### Added
- Additional parameters for session start request

## [6.5.10] - 2020-08-20
### Fixed
- Additional check for wrong fastdata responses

## [6.5.9] - 2020-06-16
### Added
- `content.transportFormat` and `content.streamingProtocol` options and getters from plugin
- Option `content.subtitles` reported in start and pings, as entities
- Totalbytes feature: `content.sendTotalBytes`and `content.totalBytes` options, and getTotalBytes in plugin
### Fixed
- Option `user.type`

## [6.5.8] - 2020-06-05
### Fixed
- Finished flag not resetting properly when task stopped

## [6.5.7] - 2020-05-20
### Added
- `parse.manifest` option

## [6.5.6] - 2020-05-08
### Fixed
- Removed beat stop when video view ends
- Player was not 'resetting' in case of changing it

## [6.5.5] - 2020-02-10
### Added
- adManifest event
- `ad.creativeId`, `ad.breaksTime`, `ad.expectedPattern`, `ad.provider`, `ad.givenAds`, `ad.givenBreaks` options

## [6.5.4] - 2020-01-28
### Fixed
- Player being registered only once
### Added
- ErrorMetadata parameter in errors
### Removed
- Mediaduration from jointime event

## [6.5.3] - 2019-12-24
### Fixed
- Negative join time in case of opening vie with /start request ( /init was working fine)

## [6.5.2] - 2019-11-26
### Fixed
- Infinity event names was always 'event' not can be customized using 'displayName' on the object

## [6.5.1] - 2019-10-15
### Added
- IMA DAI support

## [6.5.0] - 2019-09-25
### Added
- All new ad events (break start, break end, quartile, etc)
- Session events
- Custom video events

## [6.3.5] - 2019-09-10
### Added
- New flag that lets you know when the plugin is started

## [6.3.4] - 2019-06-13
### Fixed
- Various minor fixes

## [6.3.3] - 2019-05-22
### Improved
- Several changes have been made to reduce the number or rendevouz (about 43% improvement on a 120 seconds play)

## [6.3.2] - 2019-05-08
### Updated
- Fast data url has changed
### Fixed
- Buffer detection is back the way it used to be

## [6.3.1] - 2019-04-03
### Fixed
- Proper seek and buffer detection was reverted from previous versions, now is back

## [6.3.0] - 2019-04-01
### Added
- Now is possible to delay the start event (and therefore have all the metadata ready) and have correcto joinTime
- New options: `user.email`, `content.package`, `content.saga`, `content.tvShow`, `content.season`, `content.episodeTitle`, `content.channel`, `content.id`, `content.imdbId`, `content.gracenoteId`, `content.type`, `content.genre`, `content.language`, `content.subtitles`, `content.contractedResolution`, `content.cost`, `content.price`, `content.playbackType`, `content.drm`, `content.encoding.videoCodec`, `content.encoding.audioCodec`, `content.encoding.codecSettings`, `content.enconding.codecProfile`, `content.encoding.containerFormat`, `ad.customDimension.x`, `user.obfuscateIp` and `user.type`
- New option alias: `user.name`, `user.anonymousId` and `content.customdimension.x`instead of `customDimension.x`
- Now our Roku sdk makes the HDMI cable on roku optional, totally wireless
## [6.2.0] - 2019-03-27
### Added
- App dimensions as options
- Add fingerprint with youboraId
- Account code now send on every request
- DeviceInfo now can be overriden from the options
### Improved
- Buffer detection
- Init is send automatically now
- AdInit is send automatically now
### Fixed
- Flags not reseting properly on stop
### Deprecated
- Extraparams are rename to customDimension.N
- title2 now is named program

## [6.1.4] - 2019-02-19
### Fixed
- Flags are not resetting anymore unless they ware changed again

## [6.1.3] - 2019-01-03
### Fixed
- Correct join time for more than one ad
- Add /adError request

## [6.1.2] - 2018-11-12
### Fixed
- Now buffer and seek are not mixed together and are reported correctly

## [6.1.1] - 2018-11-07
### Added
- Add new option parameters. smartswitch.configcode, smartswitch.groupcode, smartswitch.contractcode
- Option to disable error reporting just in case retries are being used)
### Fixed
- Stop being omited when using init

## [6.1.0] - 2018-10-15
### Added
- Add /init request

## [6.0.4] - 2018-10-26
### Fixed
- Now join time is calculated properly even if there is more than one ad
- Add /adError request support

## [6.0.3] - 2018-10-15
### Fixed
- Add pauseDuration where it was missing

## [6.0.2] - 2018-10-08
### Fixed
- Fix unobserving events

## [6.0.1] - 2018-08-08
### Fixed
- Wrong key in data and start calls, replaced with "system"

## [6.0.0] - 2018-06-01
### Added
- Ad tracking
